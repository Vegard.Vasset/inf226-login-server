# 2A 
## Design considerations
## Weaknesses: 

- The application is very vulnerable to SQL injections, both in the login page and in the message sending page. 
- Showing the user the complete sql query that is sent to the database, when logged in is not a good policy and the potential attacker can see the the whole query that is beeing sent.
- The application has a really messy structure. Everything resides in one file: app.py. The CSS and JS code could also be seperated into seperate files from the HTML. There are also no folder structure. This makes it hard for future devolpment, and can lead to more insecure design choices. 
- The users and their passwords are stored in a plaintext dict, meaning that if a leak occurs, people can read them directly. 
- Login system do not verify user and password. Therefore atttackers can enter any password they like.
- The secret key used to to store encrypted session data is not generated at random, and is stored in plaintext. Meaning that if it gets brute forced a hacker could use it to expose the application.
- Cookies are not covered by same origin policy making it vulnerable to Cross Site Request forgery
- Users can enter any name they like in the "From" form making them appear to be anyone they like. 
- CSP header is not set which can prevent which sources content are allowed from, and violations are not reported back to the server. 
- The CSRF token is not set. which can lead to a vulnerability for Cross site request forgery.

# 2B
## Design changes and additions: 

- Split up the difference parts of the app.py file and made the relevant code stay together in seperate files and removed a lot of unused code.
- Made JS and CSS code in seperate files. Moved CSS, image files, and JS file into static folder. Removed unused CSS
- Close the SQL Injection security hole when sending messages. To do this I changed the send function to use prepared sql statements. 
- Removed SQL queries from the UI, as it may help attackers performing injection attacks. Did this by removing the JavaScript/Python code that printed it out to the screen.
- Made the encryption key for session data use the library secrets to generate a key.
- Stored users Bob and Alice in the db instead, together with their password hashes and the salt used.
- Removed the dict containing users and their passwords exposed in plain text.
- Implemented a password checking method that gets the hash and salt that is stored with the user in the database. Then hashes the input password with the correct salt, and compare them to the hash in the database, if they are equal user can proceed. If username or password is wrong user is redirected to login page. 
- Implemented a new form where users could create a new User, used flask forms with an extra field to confirm the password. And set it up to insert the new user created into the database
- Created Validation rules inside the forms file, like fields cannot be empty, passwords must be of a certain length, and when asked to confirm the password they must be equal. Minimizing the password length a user can use increases the security and together with the salted hashes stored makes the application really secure if a leak should happen.
- Added CSRF token to the forms, to prevent CSRF attacks also needed to include it in the header when using JavaScript fetch.
- Created a log out option in the apps interface without the possiblity of using the back arrow to go back into the application
- Set the CSP header of the application to control which sources content are allowed to come from. Helps preventing XSS attacks.
- Made sure users could only see messages sent to them, and not anyone elses.
- Added loginrequired parameter to functions dealing with sending and showing messsages
- Added feature to post annoncments
- Added timestamp to the messages
- Implemented is_safe_url function
- Set cookie to use the SameSite attribute strict
- (For future development I would have added better UI, possibility to answer messages, implemented the request_loader etc..)

## Features

The app I have created called Chatty is a simple messaging app used to communicate between the users of the system. You are able to log in with your own credentials, or create your very own user. When you enter the application itself you have the possibility to send message to whoever you like in the system. You can view the messages sent to you, where you get information on who sent it and when it got sent. You also have the possiblity to post announcment to every user on the system. Which will be shown sliding at the top of the screen. Lastly you have the oppurtunity to log out of the application. And when you have exited the system, and can log in again.

## Instructions on how to demo/test:

To try out the application you clone the project from gitlab down to your own machine. Then you open the terminal and make sure to navigate to the folder where the project resides. Then you type: flask run. This will set up the server for you. Then you go the web page on adress: http://127.0.0.1:5000. Here you will be met with a login page. There are already two users in the system you can use for testing: (username: bob, password: bananas) or (username: alice, password: password123). However if you want to, you can create your own user. The username you type cannot be in the system already and your password has to be 12 characters minimum. When you have logged in, your From field is already filled in for you (you cannot change this). Then you can choose what user you want to send a message to from the drop down menu, and press the send button. If you want to view all the messages sent to you, you press the show all button. You can also post anouncements, you do this by pressing the Post button. Announcments will be sliding over the top of the screen. Finally when you have had enough fun on the app, you press the log out button to log yourself out.


## Technical details on implementation:

In this application I have implemented both a backend and frontend. On the frontend there is used HTML, CSS and JavaScript, for the backend Python is used together with the web framework flask. To store the information needed for the application an SQLite database is used. 

### Technical details regarding security: 

Regarding the possibility of SQL-injections I implemented the use of prepared SQL-injections. This makes sure that the SQL-code is pre compiled and thereby seperating it from the data the user is supposed to type in as input.

Secure design of the code is also very important when it comes to security. To improve my design I split the different functions all previously residing in app.py into functions(where all my utility/helper functions are), database(where the code for creating the database is), forms (where the different types of FlaskForm are) and messageAPI where all the functions that deals with messaging are. I also created two new folders. One named static where images, javascript code and css code are. And a folder named templates where all my HTML code lies. Keeping a good and clear structure makes it easier to spot bugs that can lead to security vulnerabilites, and makes it easier to avoid security pitfalls in later development.

For storing passwords I have used a library called hashlib to make a sha-512 hash of the passwords stored I have also added a salt value to the password using the the secrets library to generate a random salt value which is also stored in the database together with the hashed password. So when the user types in their password it is then hashed with the salt in the database and compared to the hash stored there. This is a lot more secure than to store things in plaintext, using salt makes it harder to brute force the password if it should occur in a leak from the database. 

When regarding passwords I have also forced the user to choose a password at least 12 characters long, which forces security my making it harder to figure out users passwords. The best thing to do here is also force the use of some special signs and uppercase letters. 

I have enabled CSRF tokens in the application. This is a feature that prevents Cross-Site-forgery attacks because it becomes impossible for an attacker to create a fully valid HTTP request for the victim. This is done by generating a unique secret value that the server and client exchanges and knows. So that later the server expects the token to be included.

The secrets library is used to generate a secure random key for the app.secret_key. This makes sure that when the session cookie is signed wit this key it is very hard for an attacker to do anything malicious like pretending to be the user thats logged in. 

The CSP(Content security policy) header is set for the application. This is a feature helping prevent XSS attacks. It creates a way to declare approved origins of content that the browser should be allowed to load on the specific website.

The SameSite attributes for is set to 'Strict' for the session cookies. This makes sure that the cookie is only sent if it is initated from the same origin. This helps prevent cross-origin information leakage

## Answer to questions:

### The threat model 

Anyone who have the capability and want to, might attack the application. As we have looked into previously there are a lot of things an attacker can do. 

He can for example do an SQL injection extracting information from the database. This can lead to a breach of Confidentiality as any unauthorized user can view content he should not. Integrity is also at risk as anyone can change the information stored. Availability is also at risk as an attacker could delete things from the database. So SQL injections is a high danger in this application

XSS attacks are also highly possible as there are no real protection against it. This can lead to users running harmful scripts. The script can be designed to cause any sort of harmful operations and can potentially harm all three foundations of the CIA . Also authenticity and accountability can be breached as an attacker can make sure that the harmful script is run from another users session.

CSRF is another attack that could be performed. Neither here there are any real protection. An attacker could send a request from the victims browser to the server pretending to be the the victim. Now the attacker could do all sort of things. And can potential be breach of all the same things as XSS attacks. 

Authentication is also broken as anyone who knew one username in the system could be able to enter the application. Therefore an attacker could easily get in and pretending to be a legitimate user.

So to concludere there are a lot of opportunities for an attacker to cause harm. And there are also many things you cannot sensibly protect against, for example in terms of Social Engineering, you cannot protect against what users of the system might expose to other possible attackers. Large bot net attacks can also for example happen (DDOS attack) bringing down the server etc.. However in terms of this application with very little information stored at the moment, the damage the attacker could do is limited as opposed to a bank or a larger social media site like Facebook.  

One thing we have not considered is the encryption that goes on between the server and the users. If not encrypted correctly the messages can become vulnerable to man in the middle attacks 

### What are the main attack vectors ?

The main attack vectors of the application is the login and searching fields which can be exploited by SQL injection or XSS attacks, look above for details about the specific attacks 

### What should we do to protect ?

- Use prepared sql-statements
- Store password as hashes with salt
- Verification of passwords 
- Structure and design the code better
- Use CSRF token
- Use SameSiteAttribute for cookies
- Create CSP header

For more detailed explenation on this look in the technical details part

### What is the acess control model ?

The access control model for the application was mostly intended to be and most like the Mandatory AccessControl. Where users cannot change anything and the administrator decide everything. Which was the case regarding everyone could see everything that was sent. However users could decide who they wanted to send things to (at least shown in the message) so it is not completely mandatory. However since the authentication of users were completely broken before I fixed it, everyone could log in to whoever user they wanted to. Therefore the system actually had no acess control at all. Now that the authentication is fixed, and users can deicide who they want to send messages to and only view the messages that has been sent to them we have something more like a Discretionary Access Control. Following this principle users can determine who has acess to their objects (in this case the messages). This access control protects users from viewing and changing unauthorized resources.

### How can we know that security is good enough ?

To know when a system is secure enough can be very hard. There will always be a possibility that there exist a security related issue, and if it is not in your own system. It can be in librararies, the server you are hosting the application, your users might compromise the system etc... So you can never know for sure, but being able to identify the most important issues and attack vectors and fix them like its done with this application is a good start. Further on when using the system Traceability is important to maintain a secure system. Logging is an example of this. Loggging provides us with useful information during a systems lifecycle, and helps us identify security events. We can by this track down what happened, find out if someone was behind it, and where the event came from. Knowing this we can then decide how we want to act on it, and perphaps improving our code even further!