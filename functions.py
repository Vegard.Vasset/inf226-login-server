from app import *
import secrets
import hashlib
from urllib.parse import urlparse, urljoin
from flask import request, url_for

#### This file contains the helper functions Created 

# Function to get all user names stored in the db
def getUserNames(connection):
    names = connection.execute('SELECT userName FROM users').fetchall()
    accountnames = []
    for name in names:
        accountnames.append(name[0])
    return accountnames


# Function for hashing a password with a random salt
def hashPasswordForCreatingUser(password):
    salt = secrets.token_hex(16)
    password = password + salt
    hashed = hashlib.sha512(password.encode()) 
    return (hashed.hexdigest(), salt)


# Helper function for hashing the password user input with salt 
def hashPassword(password, salt):
    password += salt
    return hashlib.sha512(password.encode()).hexdigest()


# Function for checking if username already exists in application
def checkForSameUserName(users, username):
    for elem in users:
        if username == elem[0]:
            return True
    return False

# Function for checking that redirect target lead to the same server
def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc