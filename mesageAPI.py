from flask import Flask, abort, request, send_from_directory, make_response, render_template, session
from werkzeug.datastructures import WWWAuthenticate
from database import *
from json import dumps, loads
from apsw import Error
from markupsafe import escape
from functions import *
from flask_login import login_required, login_user, logout_user, fresh_login_required, current_user
from datetime import datetime
time_format ='%Y-%m-%d %H:%M:%S'


# Import app to get the flask app and db connection
from app import *


# Here are all the functions related to messaging, JS code can  use fetch on these. 
@app.get('/showAll')
@login_required
def search():
    receiver = request.args.get('q')  
    try:
        c = conn.execute('SELECT sender, message, time FROM messages WHERE receiver = ?', (receiver,))
        rows = c.fetchall()
        result = 'Messages:\n'
        for row in rows:
            result = f'{result}    {dumps(row)}\n'
        c.close()
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

@app.route('/postAnnouncement', methods=['POST','GET'])
@login_required
def post():
    try:
        sender = request.args.get('sender') 
        message = request.args.get('message') 
        if not sender or not message:
            return f'ERROR: missing announcment or sender'
        conn.execute('INSERT INTO announcements (author, text) VALUES (?, ?)', (sender, message))
        return f'Message sent!'
    except Error as e:
        return f'ERROR: {e}'

@app.route('/send', methods=['POST','GET'])
@login_required
def send():
    try:
        sender = request.args.get('sender') 
        message = request.args.get('message') 
        receiver = request.args.get('receiver')

        if not sender or not message or not receiver:
            return f'ERROR: missing sender or message'
        timestamp = datetime.now().strftime(time_format)
        conn.execute('INSERT INTO messages (sender, message, receiver, time) VALUES (?, ?, ?, ?)', (sender, message, receiver, timestamp))
        return f'Message sent!'
    except Error as e:
        return f'ERROR: {e}'

@app.get('/announcements')
@login_required
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        c = conn.execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
        return {'data':anns}
    except Error as e:
        return {'error': f'{e}'}