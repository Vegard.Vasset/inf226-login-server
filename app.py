from flask import Flask, request, send_from_directory, render_template, session
import flask
import secrets
from database import *
from forms import CreateUserForm, LoginForm
from json import dumps, loads
from base64 import b64decode
import apsw
from pygments import highlight
from pygments.lexers import SqlLexer
from pygments.formatters import HtmlFormatter
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token;
from threading import local
from flask_wtf.csrf import CSRFProtect
from functions import *

tls = local()
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
conn = initializeDb()

# Set up app
app = Flask(__name__)

# Use CSRF tokens
csrf = CSRFProtect(app)

# The secret key enables storing encrypted session data in a cookie 
app.secret_key = secrets.token_hex(32)

app.config.update(
SESSION_COOKIE_SAMESITE = 'Strict',
SESSION_COOKIE_HTTPONLY = True,
REMEMBER_COOKIE_HTTPONLY = True
)

# Set CSP header 
@app.after_request
def add_security_headers(resp):
    resp.headers['Content-Security-Policy']='default-src \'self\''
    return resp

# Add a login manager to the app
import flask_login
from flask_login import login_required, login_user, logout_user, fresh_login_required, current_user
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"


# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    receivers = []

# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):
    # For a real app, we would load the User from a database or something
    user = User()
    user.id = user_id
    user.receivers = getUserNames(conn)
    session['username'] = user.id
    return user

@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'static/favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'static/favicon.png', mimetype='image/png')

@app.route('/index.js')
def index_js():
    return send_from_directory(app.root_path, 'static/index.js', mimetype='text/javascript')

@app.route('/index.css')
def index_css():
    return send_from_directory(app.root_path, 'static/index.css', mimetype='text/css')

@app.route('/')
@app.route('/index.html')
@fresh_login_required
def index_html():
    return render_template('./index.html')

@app.route('/createUser.html', methods=["GET", "POST"])
def createUser():
    form = CreateUserForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
       userName = form.username.data.lower()
       password = form.password.data
       confirmPassword = form.confirmPassword.data
       users = conn.execute('SELECT userName from users').fetchall()
       if(checkForSameUserName(users, userName)):
            return render_template('./createUser.html', form=form)
       hashedPassword = hashPasswordForCreatingUser(password)
       conn.execute('INSERT INTO users (username, password, salt) VALUES (?, ?, ?)', (userName,hashedPassword[0],hashedPassword[1]))
       next = flask.request.args.get('next')

       if not is_safe_url(next):
                return flask.abort(400)
       return flask.redirect(next or flask.url_for('login', form=form))

    errors = [{'field': key, 'messages': form.errors[key]} for key in form.errors.keys()] if form.errors else []

    return render_template('./createUser.html', 
        form=form, 
        errors=errors
    )

@app.route('/login.html', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data.lower() # Lowers the username as usernames often are not case sensetive
        password = form.password.data
        try:
            uPassword = conn.execute('SELECT password FROM users WHERE userName=?', (username,)).fetchall()[0][0] #Extracting users password from database
            salt = conn.execute('SELECT salt FROM users WHERE userName=?', (username,)).fetchall()[0][0] # Extracting users salt from database
        except IndexError as e: # If username not in database return to index page
            return render_template('./login.html', form=form)
        password = hashPassword(password, salt) #Hash the input password with correct salt
        if password == uPassword: # Compare input password hash with database 
            user = user_loader(username)
            
            # automatically sets logged in session cookie
            login_user(user)

            flask.flash('Logged in successfully.')

            next = flask.request.args.get('next')
    
            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if not is_safe_url(next):
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('index_html'))
    return render_template('./login.html', form=form)

@app.route("/logout", methods=['GET','POST'])
@login_required
def logout():
    logout_user()
    session.pop('username', None)
    next = flask.request.args.get('next')
    if not is_safe_url(next):
                return flask.abort(400)
    return flask.redirect(flask.url_for('index_html'))

## Get all the functions related to messaging
from mesageAPI import  *