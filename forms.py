from email import message
from tokenize import PseudoExtras
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import EqualTo, DataRequired, Length


# Forms class with a lot of validation rules like data required, minimuum length of password, and both password matches 
# 


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Submit')

class CreateUserForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired(), Length(min=12), EqualTo('confirmPassword', message="Passwords do not match!")])
    confirmPassword = PasswordField('ConfirmPassword', validators=[DataRequired()])
    submit = SubmitField('Submit')