import sys
import apsw
from apsw import Error

def initializeDb():
    try:
        conn = apsw.Connection('./tiny.db')
        c = conn.cursor()
        c.execute('''CREATE TABLE IF NOT EXISTS messages (
            id integer PRIMARY KEY, 
            sender TEXT NOT NULL,
            receiver TEXT NOT NULL,
            message TEXT NOT NULL,
            time TEXT NOT NULL);''')
        c.execute('''CREATE TABLE IF NOT EXISTS announcements (
            id integer PRIMARY KEY, 
            author TEXT NOT NULL,
            text TEXT NOT NULL);''')
        c.execute('''CREATE TABLE IF NOT EXISTS users (
            userName TEXT NOT NULL PRIMARY KEY,
            password TEXT NOT NULL,
            salt TEXT NOT NULL);''')
        
        # Code used once to insert users already for testing in the system in this case Bob and alice, together with their password hashes and salt
        if c.execute('SELECT COUNT(*) FROM users').fetchone()[0] == 0:
            c.execute('INSERT INTO users (username, password, salt) VALUES (?, ?, ?)', ('bob','66ae4a4d65d563dad8e512d195602ec37fbcb248ae2fe2b96acb2bf9348ef21ab715d583e89b1bc3b8ce0d662db8f42f91e1fcfb91bfec4ca9ee95cc2be40092','e8df2696b1b61e9015cffe1888ae6ea8'))
            c.execute('INSERT INTO users (username, password, salt) VALUES (?, ?, ?)', ('alice','e41bd3e645e1fe90d6480dc87d24af4669a2f7f3d106a53f2442d651061724ea74176579dc30122dd1b16a84d9050fee8c2255a4765b9aed4f326b593f9f5309','614b5567a14cc75775ee191c2e90c31d'))
        return conn
    except Error as e:
        print(e)
        sys.exit(1)
