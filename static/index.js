var reqId = 0;
var anchor = document.getElementById('anchor');
var senderField = document.getElementById('sender');
var receiverField = document.getElementById('receiver')
var messageField = document.getElementById('message');
var sendBtn = document.getElementById('sendBtn');
var showAllMessagesBtn = document.getElementById('showAllMessagesBtn');
var postAnnouncementsBtn = document.getElementById('postAnnouncements')
var logOutBtn = document.getElementById('logOutBtn');
var output = document.getElementById('output');
var header = document.getElementById('header');
var csrftoken = document.getElementById('csrf_token').value


var checkAnnouncements = async () => {
    res = await fetch('/announcements');
    anns = await res.json();
    if (anns && Array.isArray(anns.data)) {
        const elts = [];
        anns.data.forEach((element, idx) => {
            if (idx > 0) {
                const node = document.createElement('li');
                node.textContent = '  …  ';
                elts.push(node);
            }
            const node = document.createElement('li');
            node.textContent = 'Announcement: '
            node.textContent += `${element.message || ''}`;
            node.textContent += ' From: '
            node.textContent += `${element.sender || ''}`;
            elts.push(node);
        });
        header.replaceChildren(...elts);
    }
};
var showAllMessages = async (query) => {
    const id = reqId++;
    const q = `/showAll?q=${encodeURIComponent(query)}`;
    res = await fetch(q);
    const body = document.createElement('p');
    body.innerHTML = await res.text();
    output.appendChild(body);
    body.scrollIntoView({block: "end", inline: "nearest", behavior: "smooth"});
    anchor.scrollIntoView();
    checkAnnouncements();
};
var send = async (sender, message, receiver) => {
    const id = reqId++;
    const q = `/send?sender=${encodeURIComponent(sender)}&message=${encodeURIComponent(message)}&receiver=${encodeURIComponent(receiver)}`;
    res = await fetch(q, { method: 'post', headers: {
        'X-CSRF-TOKEN': csrftoken
    } }); // Include the csrftoken when doing the fetch to the sendAPI
    const body = document.createElement('p');
    body.innerHTML = await res.text();
    output.appendChild(body); 
    body.scrollIntoView({block: "end", inline: "nearest", behavior: "smooth"});
    anchor.scrollIntoView();
    checkAnnouncements();
};

var postAnnouncement = async (sender, message) => {
    const id = reqId++;
    const q = `/postAnnouncement?sender=${encodeURIComponent(sender)}&message=${encodeURIComponent(message)}`;
    res = await fetch(q, { method: 'post', headers: {
        'X-CSRF-TOKEN': csrftoken
    } }); // Include the csrftoken when doing the fetch to the sendAPI
    const body = document.createElement('p');
    body.innerHTML = await res.text();
    output.appendChild(body); 
    body.scrollIntoView({block: "end", inline: "nearest", behavior: "smooth"});
    anchor.scrollIntoView();
    checkAnnouncements();
};

// Method for fetching logout function and restrict going back to app without being loged in
var logout = async () => {
    const id = reqId++;
    const q = 'logout'
    res = await fetch(q);
    location.reload()
}

showAllMessagesBtn.addEventListener('click', () => showAllMessages(senderField.value));
sendBtn.addEventListener('click', () => send(senderField.value, messageField.value, receiverField.value));
logOutBtn.addEventListener('click', () => logout())
postAnnouncementsBtn.addEventListener('click', () => postAnnouncement(senderField.value, messageField.value))
checkAnnouncements();